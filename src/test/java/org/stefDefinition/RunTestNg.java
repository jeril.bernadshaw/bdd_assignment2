package org.stefDefinition;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
 
@CucumberOptions(tags = "", features = "src/test/resources/feature", glue = "org.StefDefinition",plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)

public class RunTestNg extends AbstractTestNGCucumberTests{

}


